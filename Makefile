all:
	gcc -lGL -lglut -lGLU main.cpp chip8.cpp -o chip8

debug:
	gcc -g -O0 -lGL -lglut -lGLU main.cpp chip8.cpp -o chip8Debug
