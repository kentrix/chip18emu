#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdio.h>

class Chip8 {
    private:
        unsigned short opcode;
        unsigned char memory[4096]; // 4KiB Memory
        unsigned char reg[16]; // 16 registers
        unsigned short I; // Index register
        unsigned short pc;
        unsigned char delayTimer;
        unsigned char soundTimer;
        unsigned short stack[24]; // 24 deep stack ptr
        unsigned short sp;
        unsigned char key[16]; 
        bool drawFlag;
    public:
        unsigned char gfx[64 * 32];
        int loadFile(char* path);
        int reset();
        int runOneTick();
        void unsetKeys();
        bool getDrawFlag() const;
        void maskKeyFor(unsigned char key);
        void resetDrawFlag();
        
        Chip8() { drawFlag = false; }
        ~Chip8() { }
};
