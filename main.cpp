#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "chip8.hpp"

#define OUTPUT_X 256
#define OUTPUT_Y 128
#define OUTPUT_SIZ 4

static Chip8 chip8 = Chip8();

void keyFunc(unsigned char key, int a, int b) {
    chip8.maskKeyFor(key);
}

void initGL(int argc, char** argv) {

    glutInit(&argc, argv);
    glClearColor(.0, .0, .0, 0);
    glClear(GL_DEPTH_BUFFER_BIT);
    glutInitWindowSize(OUTPUT_X, OUTPUT_Y);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutKeyboardFunc(keyFunc);
    glutCreateWindow("CHIP8_EMU");

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, OUTPUT_X, 0, OUTPUT_Y);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glFlush();
}


void draw() {

    glutPostRedisplay(); // Immediately redraw

    chip8.runOneTick();

    if(!chip8.getDrawFlag()) {
        return;
    }

    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1., 1., 1.);
    for(int i = 0; i < 64*32; i++) {
        if(*(chip8.gfx+i)) {
            float y = (i / 64) * OUTPUT_SIZ;
            float x = (i % 64) * OUTPUT_SIZ;
            glBegin(GL_QUADS);
                glVertex2f(x, y);
                glVertex2f(x+OUTPUT_SIZ, y);
                glVertex2f(x+OUTPUT_SIZ, y+OUTPUT_SIZ);
                glVertex2f(x, y+OUTPUT_SIZ);
            glEnd();
        }
    }
    glFlush();
    chip8.resetDrawFlag();
    chip8.unsetKeys();
}

int main(int argc, char** argv) {
    if(argc < 1) {
        printf("chip8 GAME");
    }
    initGL(argc, argv);

    chip8.reset();
    chip8.loadFile(*(argv+1));

    glutDisplayFunc(draw);
    glutKeyboardFunc(keyFunc);
    glutMainLoop();

    return 0;
}

