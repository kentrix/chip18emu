#include "chip8.hpp"


const unsigned char chip8_fontset[80] =
{ 
  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
  0x20, 0x60, 0x20, 0x20, 0x70, // 1
  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

int Chip8::loadFile(char* path) {
    FILE* fp = fopen(path, "r");
    char* buffer = (char*)malloc(sizeof(char) * 4096);
    int ret = fread(buffer, sizeof(char), 4096, fp);
    // load into memory
    for (int i = 0; i < ret; i++){
        memory[i + 512] = *(buffer+i);
    }
    free(buffer);
    return 1;
}

bool Chip8::getDrawFlag() const{
    return drawFlag;
}

void Chip8::unsetKeys() {
    for(short i = 0; i < 16; i++) {
        key[i] = 0;
    }
}

void Chip8::resetDrawFlag() {
    drawFlag = false;
}

void Chip8::maskKeyFor(unsigned char k) {
    if (k >= '0' && k <= '9') {
        key[k-'0'] = 1;
    } else if ( k >= 'A' && k <= 'F') {
        key[k-'A'+10] = 1;
    }
}

int Chip8::reset() {
    opcode = 0;
    memset(memory, 0, sizeof(char) * 4096);
    memset(reg, 0, sizeof(char) * 16);
    I = 0;
    pc = 0x200; // pc starts at 0x200
    memset(gfx, 0, sizeof(char) * 64 * 32);
    delayTimer = 0;
    soundTimer = 0;
    memset(stack, 0, sizeof(short) * 24);
    sp = 0;
    memset(key, 0, sizeof(char) * 16);
    drawFlag = false;
    //TODO: LOAD fontset
    for(int i = 0; i < 80; ++i)
        memory[i] = chip8_fontset[i];	
    return 0;
}

int Chip8::runOneTick() {
    opcode = memory[pc] << 8 | memory[pc+1];

    //for(int i = 0; i < 16; i++) {
        //if(key[i]) 
            //printf("%d -> %d ", i, key[i]);
    //}
    //printf("\n");
    
    switch(opcode & 0xF000) {
        case 0x0000: 
            if(opcode == 0x00EE) {
                // RET
                if(sp < 0) {
                    printf("FATAL: Return at the bottom of the stack\n");
                }
                pc = stack[sp-1];
                sp--;
            }
            else if (opcode == 0x00E0) {
                memset(gfx, 0, sizeof(char) * 64 * 32);
                drawFlag = true;
            } else {
                // RCA 1802
            }
            break;
        case 0x1000:
            pc = opcode & 0x0FFF;
            break;
        case 0x2000:
            stack[sp] = pc; // Store current pc on stack
            sp++; // increment sp
            if(sp >= 24) {
                printf("FATAL: Stack overflow\n");
            }
            pc = opcode & 0x0FFF;
            // Call
            break;
        case 0x3000:
            if((opcode & 0x00FF) == (reg[(opcode & 0x0F00) >> 8])) {
                pc += 4;
            } else 
                pc += 2;
            break;
        case 0x4000:
            if((opcode & 0x00FF) != (reg[(opcode & 0x0F00) >> 8])) {
                pc += 4;
            } else 
                pc += 2;
            break;
        case 0x5000:
            if((reg[(opcode & 0x00F0) >> 4]) == (reg[(opcode & 0x0F00) >> 8])) {
                pc += 4;
            } else
                pc += 2;
            break;
        case 0x6000:
            // Assignment constant
            reg[(opcode & 0x0F00) >> 8] = opcode & 0x00FF;
            pc += 2;
            break;
        case 0x7000:
            // Add
            reg[(opcode & 0x0F00) >> 8] += opcode & 0x00FF;
            pc += 2;
            break;
        case 0x8000:
            switch (opcode & 0x000F) {
                case 0:
                    // Reg assign
                    reg[(opcode & 0x0F00) >> 8] = reg[(opcode & 0x00F0) >> 4];
                    break;
                case 1:
                    // bw or
                    reg[(opcode & 0x0F00) >> 8] |= reg[(opcode & 0x00F0) >> 4];
                    break;
                case 2:
                    // bw and
                    reg[(opcode & 0x0F00) >> 8] &= reg[(opcode & 0x00F0) >> 4];
                    break;
                case 3:
                    // bw xor
                    reg[(opcode & 0x0F00) >> 8] ^= reg[(opcode & 0x00F0) >> 4];
                    break;
                case 4:
                    if(reg[(opcode & 0x00F0) >> 4] > (0xFF - reg[(opcode & 0x0F00) >> 8]))
                        reg[0xF] = 1; //carry
                    else
                        reg[0xF] = 0;
                    reg[(opcode & 0x0F00) >> 8] += reg[(opcode & 0x00F0) >> 4];
                    break;
                case 5:
                    if(reg[(opcode & 0x00F0) >> 4] < (reg[(opcode & 0x0F00) >> 8]))
                        reg[0xF] = 0; // borrow
                    else
                        reg[0xF] = 1;
                    reg[(opcode & 0x0F00) >> 8] -= reg[(opcode & 0x00F0) >> 4];
                    break;
                case 6:
                    reg[0xF] = reg[(opcode & 0x00F0) >> 4] & 0x1;
                    reg[(opcode & 0x0F00) >> 8] = reg[(opcode & 0x00F0) >> 4] >> 1;
                    break;
                case 7:
                    if(reg[(opcode & 0x00F0) >> 4] > (reg[(opcode & 0x0F00) >> 8]))
                        reg[0xF] = 0; // borrow
                    else
                        reg[0xF] = 1;
                    reg[(opcode & 0x0F00) >> 8] = reg[(opcode & 0x00F0) >> 4]
                        - reg[(opcode & 0x0F00) >>8];
                    break;
                case 0xE:
                    reg[0xF] = reg[(opcode & 0x00F0) >> 4] & 0x80;
                    reg[(opcode & 0x0F00) >> 8] = reg[(opcode & 0x00F0) >> 4] << 1;
                    break;
            }
            pc += 2;
            break;
        case 0x9000:
            if((reg[(opcode & 0x00F0) >> 4]) != (reg[(opcode & 0x0F00) >> 8])) {
                pc += 4;
            } else
                pc += 2;
            break;

        case 0xA000:
            I = opcode & 0xFFF;
            pc += 2;
            break;

        case 0xB000:
            pc = opcode & 0xFFF + reg[0x0];
            break;

        case 0xC000:
            reg[(opcode & 0x0F00) >> 8] = rand() | (opcode & 0x00FF);
            pc += 2;
        
        case 0xD000:
            {
                unsigned short x = reg[(opcode & 0x0F00) >> 8];
                unsigned short y = reg[(opcode & 0x00F0) >> 4];
                unsigned short height = opcode & 0x000F;
                unsigned short pixel;

                reg[0xF] = 0;
                for (int yline = 0; yline < height; yline++)
                {
                    pixel = memory[I + yline];
                    for(int xline = 0; xline < 8; xline++)
                    {
                        if((pixel & (0x80 >> xline)) != 0)
                        {
                            if(gfx[(x + xline + ((y + yline) * 64))] == 1)
                                reg[0xF] = 1;                                 
                            gfx[x + xline + ((y + yline) * 64)] ^= 1;
                        }
                    }
                }

                drawFlag = true;
            }
            pc += 2;
            break;

        case 0xE000:
            switch(opcode & 0x00FF)
            {
                // EX9E: Skips the next instruction 
                // if the key stored in VX is pressed
                case 0x009E:
                    if(key[reg[(opcode & 0x0F00) >> 8]] != 0)
                        pc += 4;
                    else
                        pc += 2;
                    break;
                case 0x00A1:
                    if(key[reg[(opcode & 0x0F00) >> 8]] == 0)
                        pc += 4;
                    else
                        pc += 2;
                    break;
                default:
                    printf("FATAL: Not a valid opcode %x\n", opcode);
                    break;
            }
            break;

        case 0xF000:
            unsigned char x = (opcode & 0x0F00) >> 8;
            switch (opcode & 0x00FF) {
                case 0x0007:
                    reg[x] = delayTimer;
                    break;
                case 0x000A:
                    printf("FATAL: Unimplemented opcode %x\n", opcode);
                    for(short i = 0; i < 16; i++) {
                        if(key[i]) {
                            reg[x] = i;
                            key[i] = 0;
                            pc += 2;
                            break;
                        }
                    }
                    break;
                case 0x0015:
                    delayTimer = reg[x];
                    break;
                case 0x0018:
                    soundTimer = reg[x];
                    break;
                case 0x001E:
                    I += reg[x];
                    break;
                case 0x0029:
                    I = reg[x];
                    break;
                case 0x0033:
                    memory[I]     = reg[(opcode & 0x0F00) >> 8] / 100;
                    memory[I + 1] = (reg[(opcode & 0x0F00) >> 8] / 10) % 10;
                    memory[I + 2] = (reg[(opcode & 0x0F00) >> 8] % 100) % 10;
                    break;
                case 0x0055:
                    for(short i = 0; i <= x; i++) {
                        memory[I + i] = reg[i];
                    }
                    break;
                case 0x0065:
                    for(short i = 0; i <= x; i++) {
                        reg[i] = memory[I + i];
                    }
                    break;
            }
            pc += 2;
            break;
    }

    if(delayTimer > 0) {
        delayTimer--;
    }

    if(soundTimer > 0) {
        soundTimer--;
    }

    return 0;

}
